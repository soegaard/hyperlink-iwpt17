# README #

This folder contains code for replicating the experiments in the IWPT'17 paper "Using hyperlinks to improve multilingual partial parsers".

### Installation and replication ###

* Install pyCNN. 
* Run run_hyperlinks*.sh for the various experiments

### Who do I talk to? ###

* soegaard@di.ku.dk